core = 6.x
api = 2

libraries[phpmailer][download][type] = "get"
; url aangepast
libraries[phpmailer][download][url] = "https://github.com/PHPMailer/PHPMailer/archive/master.zip"
libraries[phpmailer][directory_name] = "phpmailer"
libraries[phpmailer][type] = "library"

;libraries[phpmailer][patch][drupal-compatibility][url] = "http://drupalcode.org/project/smtp.git/blob_plain/2acaba97adcad7304c22624ceeb009d358b596e3:/class.phpmailer.php.2.2.1.patch"
;libraries[phpmailer][patch][drupal-compatibility][md5] = "2d82de03b1a4b60f3b69cc20fae61b76"
